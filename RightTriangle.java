public class RightTriangle {
    private double height;
    private double base;

    public RightTriangle(double h, double b) {
        this.height = h;
        this.base = b;
    }

    public double getArea() {
		
		System.out.println("Testing area: " + (1 / 2) * this.height * this.base);
        return (1.0 / 2) * this.height * this.base;
    }

    public double getPerimeter() {
        return this.height + this.base + this.getHypotenuse();
    }

    public double getHypotenuse() {
        return Math.sqrt(this.height * this.height + this.base * this.base);
    }

    public String toString() {
        return "Height: " + this.height + ", Width: " + this.base + " , Area: " + this.getArea() + " Perimeter: " + this.getPerimeter();
    }
}
